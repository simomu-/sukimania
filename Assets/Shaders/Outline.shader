﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/Outline" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
		_EdgeSize("Edge Size",  Float) = 1 
		_OutlineColor("Outline Color", Color) = (0,1,0,1)
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200

//		Pass{                                           // 1Pass  表側を描画
//		      Cull         Back                             // Back(裏) は非表示
//		      ZTest        LEqual                           // 深度バッファと比較(同等,近距離)
//
//		      CGPROGRAM                                     // Cgコード開始
//		      #include "UnityCG.cginc"                      // 基本セット
//		      #pragma target 3.0                            // Direct3D 9 上の Shader Model 3.0 にコンパイル
//
//		      #pragma vertex        vertFunc                // バーテックスシェーダーに vertFunc を使用
//		      #pragma fragment    fragFunc                  // フラグメントシェーダーに fragFunc を使用
//
//		      // Cgコード内で、使用する宣言
//		      float4 _Color;                                // color
//		      sampler2D _MainTex;                          // texture
//
//		      float4 _MainTex_ST;                           // uv
//
//		      struct v2f {                                  // vertex シェーダーと fragment シェーダーの橋渡し
//		        float4 pos      : SV_POSITION;
//		        float2 uv       : TEXCOORD0;
//		      };
//
//		      v2f vertFunc(appdata_tan v) {                 // Vertex Shader
//		        v2f o;
//		        o.uv.xy = TRANSFORM_TEX(v.texcoord, _MainTex);
//		        o.pos = mul(UNITY_MATRIX_MVP, v.vertex);    // (UNITY_MATRIX_MVP = model*view*projection) *頂点
//		        return o;
//		      }
//
//		      float4 fragFunc(v2f i) : COLOR {              // Fragment Shader
//		        return tex2D(_MainTex, i.uv.xy);
//		      }
//
//		      ENDCG                                         // Cgコード終了
//	    }

		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
		};

		half _Glossiness;
		half _Metallic;
		fixed4 _Color;

		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb;
			// Metallic and smoothness come from slider variables
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = c.a;
		}
		ENDCG

		
    //------------------------------------------------------------
	    Pass{                                           // 2Pass  裏側を描画

	      Cull         Front                            // Front(表) は非表示
	      ZTest        Less                             // 深度バッファと比較(近距離)

	      CGPROGRAM                                     // Cgコード

	      #include "UnityCG.cginc"                      // 基本セット
	      #pragma target 3.0                            // Direct3D 9 上の Shader Model 3.0 にコンパイル

	      #pragma vertex        vertFunc                // バーテックスシェーダーに vertFunc を使用
	      #pragma fragment    fragFunc                  // フラグメントシェーダーに fragFunc を使用

	                                                    // Cgコード内で、使用する宣言
	      float4 _Color;                                // color
	      sampler2D _MainTex;                           // texture
	      float _EdgeSize;
	      float4 _OutlineColor;

	      float4 _MainTex_ST;                           // uv

	      struct v2f {                                  // vertex シェーダーと fragment シェーダーの橋渡し
	        float4 pos      : SV_POSITION;
	        float2 uv       : TEXCOORD0;
	      };

	      v2f vertFunc(appdata_tan v) {                 // Vertex Shader
	        v2f o;
	        o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
	        float4 pos = UnityObjectToClipPos(v.vertex);                               // 頂点
	        float4 normal = normalize( UnityObjectToClipPos(float4(v.normal, 0)) );    // 法線
	        float  edgeScale = _EdgeSize * 0.002;                                       // Edge スケール係数
	        float4 addpos = normal * pos.w * edgeScale;                                 // 頂点座標拡張方向とスケール
	        o.pos = pos + addpos;
	        return o;
	      }

	      float4 fragFunc(v2f i) : COLOR{               // Fragment Shader
//          float4 col = tex2D(_MainTex, i.uv.xy);      // テクスチャーから得る場合
			float4 col = _OutlineColor;                  // わかりやすく青くしてます。
	        return col;
	      }

	      ENDCG                                         // Cgコード終了
	    }

	}
	FallBack "Diffuse"


}
