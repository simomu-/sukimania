﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Easing  {

    public static float CubicInOut(float time) {
        float t = Mathf.Clamp01(time);
        if ((t /= 0.5f) < 1) {
            return 0.5f * t * t * t;
        }
        return 0.5f * ((t -= 2) * t * t + 2);
    }

}