﻿using UnityEngine;
using UnityEngine.UI;

public class StageSelectScroll : MonoBehaviour {

	[SerializeField] ScrollRect m_ScrollRect;
	[SerializeField] RectTransform m_Content;
    [SerializeField] RectTransform m_ReserveContent;
    [SerializeField] float m_MoveSpeed = 10f;
    [SerializeField] int m_InitIndex = 0;
	int m_StageIndex = 0;
	int m_MaxStageCount;

	void Start () {
		m_MaxStageCount = m_Content.childCount - 1 - (m_ReserveContent.childCount == 0 ? 1 : 0);
        m_StageIndex = m_InitIndex;
        m_ScrollRect.verticalNormalizedPosition = 1.0f;
    }

    void Update () {
		float position = (float)m_StageIndex / (m_Content.childCount - 1);
		m_ScrollRect.verticalNormalizedPosition = Mathf.Lerp (m_ScrollRect.verticalNormalizedPosition, position, m_MoveSpeed * Time.deltaTime);

        //if (Input.GetKeyDown (KeyCode.RightArrow)) {
        //	MoveRight ();
        //}
        //if (Input.GetKeyDown (KeyCode.LeftArrow)) {
        //	MoveLeft ();
        //}
	}

	public void MoveUp(){
		if (m_StageIndex < m_MaxStageCount) {
			m_StageIndex++;
		}
	}

	public void MoveDown(){
		if (m_StageIndex > 0) {
			m_StageIndex--;
		}
	}

    public void AddContent(RectTransform content) {
        content.SetParent(m_ReserveContent, false);
        m_MaxStageCount = m_Content.childCount - 1;
        //SetPositionFromIndex(m_StageIndex);
    }

    public void SetPositionFromIndex(int index) {
        m_StageIndex = index;
        m_ScrollRect.verticalNormalizedPosition = (float)m_StageIndex / m_MaxStageCount;
    }
}
