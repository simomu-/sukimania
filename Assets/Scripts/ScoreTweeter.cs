﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ScoreTweeter {

    public static void Tweet(int score, int level) {
        string text = string.Format("{0}個の隙間をすり抜けました\n到達レベル:{1}\n#unity1week #unityroom #Sukimania\n", score, level);
        text += "https://unityroom.com/games/sukimania";
        string url = "http://twitter.com/intent/tweet?text=" + WWW.EscapeURL(text);
        if (Application.platform == RuntimePlatform.WebGLPlayer) {
            Application.ExternalEval("window.open(\"" + url + "\",\"_blank\")");
            return;
        }
        Application.OpenURL(url);
    }
}
    