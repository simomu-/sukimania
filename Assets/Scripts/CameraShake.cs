﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraShake : MonoBehaviour {

    [SerializeField] float shakeDecay = 0.002f;
    [SerializeField] float coefShakeIntensity = 0.3f;
    private Vector3 originPosition;
    private Quaternion originRotation;
    private float shakeIntensity;

    void Update() {
        if (shakeIntensity > 0) {
            transform.position = originPosition + Random.insideUnitSphere * shakeIntensity;
            transform.rotation = new Quaternion(
                                             originRotation.x + Random.Range(-shakeIntensity, shakeIntensity) * 2f,
                                             originRotation.y + Random.Range(-shakeIntensity, shakeIntensity) * 2f,
                                             originRotation.z + Random.Range(-shakeIntensity, shakeIntensity) * 2f,
                                             originRotation.w + Random.Range(-shakeIntensity, shakeIntensity) * 2f);
            shakeIntensity -= shakeDecay;
        }
    }

    public void Shake() {
        originPosition = transform.position;
        originRotation = transform.rotation;
        shakeIntensity = coefShakeIntensity;
    }
}
