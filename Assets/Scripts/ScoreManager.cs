﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour {

    public static ScoreManager Instance;

    int currentScore = -1;
    bool displayResult = false;
    int attainedLevel = 0;

    public int Score {
        get { return currentScore; }
    }

    private void Awake() {
        Instance = this;
    }

    private void Update() {
        if (Input.anyKeyDown && !UIManager.Instance.EnableRanking && displayResult) {
            if (!Input.GetMouseButtonDown(0)) {
                UnityEngine.SceneManagement.SceneManager.LoadScene("Game");
            }
        }
    }

    public void AddScore() {
        currentScore++;
        UIManager.Instance.SetScore(currentScore);
    }

    public void GameOver(int level) {
        attainedLevel = level;
        StartCoroutine(GameOverCoroutine());
    }

    IEnumerator GameOverCoroutine() {
        yield return new WaitForSeconds(1.2f);
        displayResult = true;
        UIManager.Instance.SetResult(currentScore);
    }

    public void Tweet() {
        ScoreTweeter.Tweet(currentScore, attainedLevel);
    }

    public void SendScore() {
        StartCoroutine(SendScoreCoroutine());
    }

    IEnumerator SendScoreCoroutine() {

        RectTransform ranking = null;
        if (!UnityEngine.SceneManagement.SceneManager.GetSceneByName("Ranking").isLoaded) {
            naichilab.RankingLoader.Instance.SendScoreAndShowRanking(currentScore);
            while (ranking == null) {
                yield return null;
                ranking = GameObject.FindWithTag("Finish").GetComponent<RectTransform>();
            }
        }
        UIManager.Instance.ShowRanking(ranking);
    }

}
