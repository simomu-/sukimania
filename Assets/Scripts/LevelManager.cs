﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour {

    [SerializeField] int levelUpBouder;
    [SerializeField] GameObject particle;
    [SerializeField] UnityEngine.PostProcessing.PostProcessingBehaviour imageEffect;
    [SerializeField] float imageEffectIntensity;

    int currentLevel = 1;
    int passingCount = -1;
    float currentIntensity;
    AudioSource audio;

    public int Level {
        get { return currentLevel; }
    }

    private void Start() {
        UIManager.Instance.SetLevel(currentLevel);
        particle.SetActive(false);
        audio = GetComponent<AudioSource>();
    }

    private void Update() {
        var setting = imageEffect.profile.vignette.settings;
        setting.intensity = Mathf.Lerp(setting.intensity, 0, Time.deltaTime * 0.8f);
        imageEffect.profile.vignette.settings = setting;
    }

    public void Pass() {
        passingCount++;
        if(passingCount >= levelUpBouder + currentLevel * 2) {
            passingCount = 0;
            currentLevel++;
            UIManager.Instance.SpeedUp();
            UIManager.Instance.SetLevel(currentLevel);

            var setting = imageEffect.profile.vignette.settings;
            setting.intensity = imageEffectIntensity;
            imageEffect.profile.vignette.settings = setting;

            audio.Play();

            particle.SetActive(true);
            StartCoroutine(ParticleDisableCoroutine());
        }
    }

    IEnumerator ParticleDisableCoroutine() {
        yield return new WaitForSeconds(1.4f);
        particle.SetActive(false);
    }


}
