﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceCreateController : MonoBehaviour {

    [SerializeField] Vector3 createPosition;
    [SerializeField] Space defalutSpacePrefab;
    [SerializeField] Space thinSpacePrefab;

    [HideInInspector] public LevelManager levelManager;

    bool canMove = true;

    public bool CanMove {
        get { return canMove; }
    }

    private void Start() {
        levelManager = GetComponent<LevelManager>();
        CreateSpace();
    }

    public void CreateSpace() {
        Space space = Instantiate((Random.Range(0.0f, 1.0f) >= 0.5f ? defalutSpacePrefab : thinSpacePrefab)) as Space;
        space.Init(this);
        space.transform.position = createPosition;
        levelManager.Pass();
        ScoreManager.Instance.AddScore();
    }

    public void Stop() {
        canMove = false;
        ScoreManager.Instance.GameOver(levelManager.Level);
    }

}
