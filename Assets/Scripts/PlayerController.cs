﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    [SerializeField] GameObject meshObject;
    [SerializeField] GameObject triggerObject;
    [SerializeField] Vector3 defaultScale = Vector3.one;
    [SerializeField] Vector3 middleSlace;
    [SerializeField] Vector3 thinScale;
    [SerializeField] float rotateStep = 20.0f;

    bool canControll = true;
    Vector3[] scales = new Vector3[3];
    int scaleIndex = 0;
    float currentAngle;

    private void Start() {
        meshObject.transform.localScale = defaultScale;
        scales[0] = defaultScale;
        scales[1] = middleSlace;
        scales[2] = thinScale;
    }

    private void Update() {

        if (canControll) {
            PlayerControll();
        }
    }

    private void PlayerControll() {
        if (Input.GetKeyDown(KeyCode.RightArrow)) {
            currentAngle -= rotateStep;
        }

        if (Input.GetKeyDown(KeyCode.LeftArrow)) {
            currentAngle += rotateStep;
        }

        float angle = Mathf.LerpAngle(meshObject.transform.eulerAngles.z, currentAngle, Time.deltaTime * 15f);
        meshObject.transform.eulerAngles = new Vector3(0, 0, angle);
        triggerObject.transform.eulerAngles = new Vector3(0, 0, currentAngle);

        if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.Space)) {
            scaleIndex += 2;
            if (scaleIndex >= scales.Length) {
                scaleIndex = 0;
            }
        }

        if (Input.GetKeyDown(KeyCode.UpArrow)) {
            scaleIndex -= 2;
            if (scaleIndex < 0) {
                scaleIndex = scales.Length - 1;
            }
        }

        Vector3 scale = scales[scaleIndex];
        meshObject.transform.localScale = Vector3.Lerp(meshObject.transform.localScale, scale, Time.deltaTime * 20f);
        triggerObject.transform.localScale = scale;
    }

    private void OnTriggerEnter(Collider other) {
        Space space = other.transform.root.GetComponent<Space>();
        if(space != null) {
            space.Stop();
            canControll = false;
        }
    }

    private void OnCollisionEnter(Collision collision) {
        Space space = collision.transform.root.GetComponent<Space>();
        if(space != null && canControll) {
            space.Stop();
            Camera.main.GetComponent<CameraShake>().Shake();
            canControll = false;
        }
    }

}
