﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleController : MonoBehaviour {

    [SerializeField] GameObject particlePrefab;
    [SerializeField] int particleNum;
    [SerializeField] float minRadius;
    [SerializeField] float maxRadius;
    [SerializeField] LevelManager levelManager;
    [SerializeField] SpaceCreateController createController;

    GameObject[] particles;

    private void Start() {
        particles = new GameObject[particleNum];
        for(int i = 0; i < particleNum; i++) {
            float angle = Random.Range(0.0f, 360.0f);
            float radius = Random.Range(minRadius, maxRadius);
            float x = Mathf.Sin(angle * Mathf.Deg2Rad) * radius;
            float y = Mathf.Cos(angle * Mathf.Deg2Rad) * radius;
            float z = Random.Range(0.0f, 22.0f);
            GameObject go = Instantiate(particlePrefab) as GameObject;
            go.transform.position = new Vector3(x, y, z);
            //go.transform.eulerAngles = new Vector3(Random.Range(0.0f, 180.0f), 
            //                                       Random.Range(0.0f, 180.0f), 
            //                                       Random.Range(0.0f, 180.0f));
            var rigid = go.GetComponent<Rigidbody>();
            rigid.angularVelocity = new Vector3(Random.Range(-5.0f, 5.0f),
                                                Random.Range(-5.0f, 5.0f),
                                                Random.Range(-5.0f, 15.0f));
            if(levelManager != null) {
                rigid.velocity = Vector3.back * (Random.Range(15.0f, 16.0f) + levelManager.Level * 0.005f);
            } else {
                rigid.velocity = Vector3.back * (Random.Range(15.0f, 16.0f));
            }
            particles[i] = go;
        }
    }

    private void Update() {
        foreach(var go in particles) {
            float cos_to_camera = Vector3.Dot(Vector3.forward, (Camera.main.transform.position - go.transform.position).normalized);
            if (cos_to_camera > 0) {
                ResetPossition(go);
            }

            if (createController != null && !createController.CanMove) {
                go.GetComponent<Rigidbody>().velocity = Vector3.zero;
            }
        }
    }

    private void ResetPossition(GameObject gameObject) {
        float angle = Random.Range(0.0f, 360.0f);
        float radius = Random.Range(minRadius, maxRadius);
        float x = Mathf.Sin(angle * Mathf.Deg2Rad) * radius;
        float y = Mathf.Cos(angle * Mathf.Deg2Rad) * radius;
        gameObject.transform.position = new Vector3(x, y, 22.0f);
    }

}
