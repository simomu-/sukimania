﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Space : MonoBehaviour {

    [SerializeField] float rotateStep = 45.0f;
    [SerializeField] float startSpeed = 10.0f;
    [SerializeField] float speedUpValue = 0.1f;
    [SerializeField] Renderer edgeRenderer;
    [SerializeField] Material passMaterial;
    [SerializeField] Material failMaterial;

    float currentSpeed;
    SpaceCreateController createManager;
    Rigidbody rigidbody;
    Transform playerTrans;
    Renderer renderer;
    Collider[] colliders;
    AudioSource audio;
    bool passed = false;

    public void Init(SpaceCreateController manager) {
        createManager = manager;
        playerTrans = GameObject.FindWithTag("Player").transform;
        colliders = GetComponentsInChildren<Collider>();
        audio = GetComponent<AudioSource>();
    }

    private void Start() {
        currentSpeed = startSpeed;
        transform.eulerAngles = new Vector3(0, 0, Random.Range(0, 8) * rotateStep);
        rigidbody = GetComponent<Rigidbody>();
        rigidbody.velocity = new Vector3(0, 0, -(currentSpeed + createManager.levelManager.Level * speedUpValue));
    }

    private void Update() {

        if (!createManager.CanMove) {
            rigidbody.velocity = Vector3.zero;
            edgeRenderer.material = failMaterial;
        }

        float cos_to_camera = Vector3.Dot(transform.forward, (Camera.main.transform.position - transform.position).normalized);
        if (cos_to_camera > 0 && !passed) {
            createManager.CreateSpace();
            passed = true;
            Destroy(gameObject, 1f);
        }

        float cos_to_player = Vector3.Dot(transform.forward, (playerTrans.position - transform.position).normalized);
        if (cos_to_player > 0) {
            foreach (var collider in colliders) {
                collider.enabled = false;
            }
            if (createManager.CanMove) {
                edgeRenderer.material = passMaterial;
            }
        }

    }

    public void Stop() {
        createManager.Stop();
        StartCoroutine(AudioStopCorotine());
    }

    IEnumerator AudioStopCorotine() {

        float time = 0;
        float deltaVolume = 1.0f;
        while(time <= 1.0f) {
            time += Time.deltaTime;
            audio.volume -= deltaVolume * Time.deltaTime;
            yield return null;
        }
        audio.Stop();
    }

}
