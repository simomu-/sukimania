﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

    public static UIManager Instance;

    [SerializeField] StageSelectScroll selectScroll;
    [SerializeField] Text speedUpText;
    [SerializeField] Text scoreText;
    [SerializeField] Text levelText;
    [Space]
    [SerializeField] Text resultText;

    bool enableRanking;

    public bool EnableRanking{
        get { return enableRanking; }
    }

    private void Awake() {
        Instance = this;
    }

    public void SpeedUp() {
        speedUpText.GetComponent<Animator>().SetTrigger("SpeedUp");
    }

    public void SetScore(int score) {
        scoreText.text = score.ToString();
    }

    public void SetLevel(int level) {
        levelText.text = string.Format("Level: {0}", (level).ToString());
    }

    public void SetResult(int score) {
        resultText.text = string.Format("Score :{0}", score);
        selectScroll.MoveDown();
    }

    public void Tweet() {
        ScoreManager.Instance.Tweet();
    }

    public void SendScore() {
        ScoreManager.Instance.SendScore();
    }

    public void ShowRanking(RectTransform ranking = null) {
;        if (ranking != null) {
            enableRanking = true;
            selectScroll.AddContent(ranking);
            Button button = ranking.Find("CloseButton").GetComponent<Button>();
            button.onClick.RemoveAllListeners();
            button.onClick.AddListener(CloseRanking);
            StartCoroutine(ShowRankingCoroutine());
        } else {
            selectScroll.MoveDown();
        }

    }

    IEnumerator ShowRankingCoroutine() {
        yield return null;
        selectScroll.MoveDown();
    }

    public void CloseRanking() {
        selectScroll.MoveUp();
        enableRanking = false; ;
    }

}
